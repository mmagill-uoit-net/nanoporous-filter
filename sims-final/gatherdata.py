import numpy as np
#import matplotlib.pyplot as plt
import pandas as pd
import os
import glob

## Coordinates from which Rgr,Rgp are calculated
## Currently sensing pore center, because rmin is calculated from there
refx=100.
refy=100.
refz=200.

# Bring all the cases together into a Pandas dataframe
casename="final"

# Columns are the values that define a given translocation
mycolumns=['N','rseed','nevent','tau','s_lt','s_fp','Rgx_lt','Rgy_lt','Rgz_lt','Rg_lt','Rgr_lt','Rgp_lt','N_cis']
df = pd.DataFrame(columns=mycolumns)


# Find all the N values for which there is data
datadirnames = np.array(os.listdir("data/"))
Ns = [dirname.split("_")[-1] for dirname in datadirnames]
Ns = np.sort(np.array(Ns,dtype=int))

# Loop over each N
for N in Ns:
    print "Gathering N=%d"%N

    # Find all the rseed values for which there is data
    ttfilenames = np.array(glob.glob("data/N_%s_%d/"%(casename,N)+"trans_time_%d-*.dat"%N))
    rseeds = [ttfilename.split(".")[0].split("-")[-1] for ttfilename in ttfilenames]
    rseeds = np.array(rseeds,dtype=int)

    # Loop over each rseed
    for rseed in rseeds:

        # Required files
        f_tt = pd.read_csv("data/N_%s_%d/"%(casename,N)+"trans_time_%d-%d.dat"%(N,rseed),header=None,delimiter=' ').values
        f_ncis = pd.read_csv("data/N_%s_%d/"%(casename,N)+"n_cis-%d-%d.txt"%(N,rseed),header=None,delimiter=' ').values
        f_rgtrns = pd.read_csv("data/N_%s_%d/"%(casename,N)+"rg_trans-%d-%d.dat"%(N,rseed),header=None,delimiter=' ').values
        f_thread = pd.read_csv("data/N_%s_%d/"%(casename,N)+"thread_indexing-%d-%d.dat"%(N,rseed),header=None,delimiter=' ').values
        f_topthread = pd.read_csv("data/N_%s_%d/"%(casename,N)+"top_thread_index-%d-%d.dat"%(N,rseed),header=None,delimiter=' ').values
        f_contact = reversed(open("data/N_%s_%d/"%(casename,N)+"part_pos_contact-%d-%d.xyz"%(N,rseed)).readlines())

        # Count how many nevents each (N,rseed) contains
        nevents = f_tt.shape[0]
        
        # If nevents>10, means some data from the previous batch tainted the file
        # This is a problem, but it'll be okay if we only take the last 10 events
        if nevents>10:
            print "\tnevents was %d"%nevents
            nevents=10 # Max 10 events
            
        # Loop over each event
        for nevent in range(nevents):
            # Count backwards to remove tainted data
            i = -1-nevent

            # This set of data was missing the correct Rg analyser
            # Have to compute by hand to make up for it
            # Reading the position file from the bottom up
            try:
                # Get radius of gyration in normal coordinates
                conformation = np.array([next(f_contact).split('\n')[0].split(' ')[1:] for fakeindex in range(N)],dtype=float)
                Rgx = np.std(conformation[:,0])
                Rgy = np.std(conformation[:,1])
                Rgz = np.std(conformation[:,2])
                Rg  = (Rgx**2 + Rgy**2 + Rgz**2)**0.5

                # Get radius of gyration in coordinates radial to the pore
                # http://tinyurl.com/hehv39s
                conformation_translated = conformation - [refx,refy,refz]
                chainCOM                = np.mean(conformation_translated,axis=0)
                theta = np.arctan(-chainCOM[2]/chainCOM[1])
                alpha = np.arctan(- (np.cos(theta)*chainCOM[1] - np.sin(theta)*chainCOM[2])/chainCOM[0] )
                R = np.array([ [np.cos(alpha), -np.sin(alpha)*np.cos(theta),  np.sin(alpha)*np.sin(theta)],
                               [np.sin(alpha),  np.cos(alpha)*np.cos(theta), -np.cos(alpha)*np.sin(theta)],
                               [      0      ,          np.sin(theta)      ,          np.cos(theta)      ]])
                conformation_rotated    = np.dot(R,conformation_translated.T).T
                Rgr = np.std(conformation_rotated[:,0])
                Rgp = (np.std(conformation_rotated[:,1])+np.std(conformation_rotated[:,2]))/2.

                # Burn headers
                next(f_contact) # Burn second xyz header
                next(f_contact) # Burn first xyz header

            except (ValueError,StopIteration):
                print "Incomplete file!"
                Rgx = -1.
                Rgy = -1.
                Rgz = -1.
                Rg  = -1.
                Rgr = -1.
                Rgp = -1.

            # Add this case to the dataframe
            df = df.append(pd.Series(np.array([N,
                                               rseed,1+nevents+i,
                                               f_tt[i,0],
                                               f_thread[i],f_topthread[i],
                                               Rgx,Rgy,Rgz,
                                               Rg ,Rgr,Rgp,
                                               f_ncis[i,0]])
                                     ,index=mycolumns),ignore_index=True)



# Save the dataframe
df.to_csv("summary_"+casename+".csv")
