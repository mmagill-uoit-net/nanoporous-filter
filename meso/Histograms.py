import numpy as np
import matplotlib.pyplot as plt


nparts = 200
nbins = 12

data = np.loadtxt('data/bin.dat')

nlines = np.shape(data)[0]
nblocks = nlines/nparts

for i in range(nblocks):

    block = data[i*nparts:(i+1)*nparts,:]

    plt.hist(block[block[:,0]==0,1],bins=range(nbins),histtype='step',normed=1,color='orange',label='N = 25')
    plt.hist(block[block[:,0]==1,1],bins=range(nbins),histtype='step',normed=1,color='cyan',label='N = 50')
    plt.hist(block[block[:,0]==2,1],bins=range(nbins),histtype='step',normed=1,color='purple',label='N = 100')
    plt.hist(block[block[:,0]==3,1],bins=range(nbins),histtype='step',normed=1,color='green',label='N = 200')
    plt.ylim([0,1])
    plt.ylabel('Fraction of Population')
    plt.xlim([0,nbins])
    plt.xlabel('Distance From Start')
    plt.legend(loc='upper right')
    plt.savefig('plots/test_%03d.png' % i)
    plt.clf()


# avconv -r 30 -i plots/test_%03d.png test.mp4
