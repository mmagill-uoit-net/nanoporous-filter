#include <stdio.h>
#include <stdlib.h>
#include <math.h>



float TransTime(float ChainLength);
float GetChainD(float ChainLength);

int Get_NthBinaryBit(int Number, int N);


int main(int argc, char* argv[]) {

  // kT, zeta, sigma, etc. all set to 1

  // FUTURE WORK: Make traversal of partice list 'magic bag', 
  // // then revert to deterministic particle type initialization

  // Declarations
  float dx; float dy; float dz;
  float UnitRand; 
  float DistToPore;

  // Inputs
  int NumChains = 100;
  float dt = 0.01;
  int nT = 400000; // snp
  //int nT = 1000000; // cnp
  //int nT = 10000; // debug
  int indr, indz;
  float curposr;
  float curFx,curFy;
  float curFr,curFz;

  // Output files
  FILE* fvmd = fopen("data/multipore.xyz","w");
  FILE* fbin = fopen("data/histogram.dat","w");

  // Geometry: Cubic box with pore in one side, origin in corner
  float AnteRadius = 13.0;
  float AnteHalfWidth = 13.0;
  float AnteWidth = 2*AnteHalfWidth;
  float PoreRadius = 1.3;
  float Radius_PoreCapture = 3.0;
  float WallHalfWidth = 4.0; // tpore parameter in field solver
  float WallWidth = 2*WallHalfWidth;
  float porex = 0.;
  float porey = 0.;
  float porez = AnteWidth+WallHalfWidth;
  
  // Arrays
  // Positions are in wrapped coordinates
  float* posx=malloc(sizeof(float)*NumChains); float* posy=malloc(sizeof(float)*NumChains); float* posz=malloc(sizeof(float)*NumChains);
  int* chln = malloc(sizeof(int)*NumChains);  // chain lengths
  int* type = malloc(sizeof(int)*NumChains);  // type for coloring
  int* trns = malloc(sizeof(int)*NumChains);  // number of translocations
  int* stat = malloc(sizeof(int)*NumChains);  // status flag: 0 normal, else frozen

  // Chain dictionary: for colouring by chain length
  char Colors[4] = {'A','E','L','Q'};

  // Initial conditions
  int curtype;
  for (int i = 0; i<NumChains; i++) {
    // Point Source at t=0
    //    posx[i] = -AnteRadius + i*(2*AnteRadius/NumChains);
    posx[i] = 0.0; posy[i] = 0.0; posz[i] = (AnteWidth+WallWidth)/2.;
    // Chain Lengths: 25% chance of 25, 50, 100, 200
    curtype = (int) floor(4* ( ((float)rand()/RAND_MAX) ) );
    type[i] = curtype;
    //chln[i] = (int) 25*pow(2, curtype);
    chln[i] = 50;
    // All particles initialized
    stat[i] = 0;
  }


  // // // Force Field
  // Loaded map is half-domain in z
  int FNr = 32; float Fdr = AnteRadius/(FNr-1);
  int FNz_load = 32; float Fdz = AnteWidth/(FNz_load-1);
  int FNz = 2*FNz_load;
  float* U = malloc(sizeof(float)*FNr*FNz);
  float* Fr = malloc(sizeof(float)*(FNr-1)*(FNz-1));
  float* Fz = malloc(sizeof(float)*(FNr-1)*(FNz-1));

  // Open Potential File
  char fvoltsname[256];
  sprintf(fvoltsname,"volts/soln_%.1f_%.1f_%.1f_%.1f_%03d_%03d.dat",
	  AnteRadius,AnteHalfWidth,PoreRadius,WallHalfWidth,FNr,FNz_load);
  FILE* fvolts = fopen(fvoltsname,"r");
  if (fvolts == 0) {printf("Voltage map missing!\n"); return 1;}

  // Load Potential
  // Loaded map goes from 0 (pore A) to -1 (middle)
  float gain = 5e-1;
  float volt;
  for (int j = 0; j<FNz_load; j++) {
    for (int i = 0; i<FNr; i++) {
      fscanf(fvolts,"%f",&volt);
      U[i + FNr*j] = gain*volt;
      U[i + FNr*(FNz-1-j)] = gain*(-2-volt); // -2-volt is for symmetry
    }
  }
  /*  
  // Print voltage (debug)
  for (int j = 0; j<FNz; j++) {
    for (int i = 0; i<FNr; i++) {
      printf("%f ",U[i+FNr*j]);
    }
    printf("\n");
  }
  */
  // Obtain Forces
  for (int j = 0; j<FNz-1; j++) {
    for (int i = 0; i<FNr-1; i++) {
      Fr[i + (FNr-1)*j] = - ( U[(i+1) + FNr*j] - U[i + FNr*j] ) / Fdr;
      Fz[i + (FNr-1)*j] = - ( U[i + FNr*(j+1)] - U[i + FNr*j] ) / Fdz;
      //      printf("%f ",Fz[i+(FNr-1)*j]);
    }
    //    printf("\n");
  }  


  // // // Main Loop
  int EqlFlag = 0; // Equilibration flag
  int nEqlTime = 50000; // Equilibration time
  nT = nT + nEqlTime; // T is still the total post-eqlbn time
  int PoreState = 0; // Nth binary bit is state of Nth hole (0 empty, 1 full)
  int nt = 0;
  int binfreq = 1000;
  int printfreq = 10;
  while ( nt < nT ) {
    //    printf("%f\n",posz[0]);
    nt++;
    if ( (EqlFlag==0) && (nt>nEqlTime) ) { EqlFlag=1; printf("Equilibrated\n"); }
    printf("\r%02d%% Completed...",(int)100*nt/nT); fflush(stdout);

    // Print Header
    if ( (EqlFlag==1) && (nt%printfreq==0) ) {
      fprintf(fvmd,"%i\n",NumChains); fprintf(fvmd,"title\n");
      //fprintf(fvmd,"%i\n",NumChains+4); fprintf(fvmd,"title\n");
      //fprintf(fvmd,"%c 0.0 -10.0 0.0\n",Colors[0]); fprintf(fvmd,"%c 5.0 -10.0 0.0\n",Colors[1]);
      //fprintf(fvmd,"%c 10.0 -10.0 0.0\n",Colors[2]); fprintf(fvmd,"%c 15.0 -10.0 0.0\n",Colors[3]);
    }

    for (int i = 0; i<NumChains; i++) {

      // Print position
      if ( (EqlFlag==1) && (nt%printfreq==0) ) {
	fprintf(fvmd,"%c %f %f %f\n",Colors[type[i]],posx[i],posy[i],posz[i]+trns[i]*(AnteWidth+WallWidth));
	if ( (nt%binfreq)==0 ) {fprintf(fbin,"%d %d\n",type[i],trns[i]);}
      }

      // If not frozen
      if ( stat[i] == 0 ) {

	// Diffusive motion
	UnitRand=sqrt(12.0)*(((float)rand()/RAND_MAX)-0.5); dx=UnitRand*pow((2.0*GetChainD(chln[i])*dt),0.5); posx[i]=posx[i]+dx;
	UnitRand=sqrt(12.0)*(((float)rand()/RAND_MAX)-0.5); dy=UnitRand*pow((2.0*GetChainD(chln[i])*dt),0.5); posy[i]=posy[i]+dy;
	UnitRand=sqrt(12.0)*(((float)rand()/RAND_MAX)-0.5); dz=UnitRand*pow((2.0*GetChainD(chln[i])*dt),0.5); posz[i]=posz[i]+dz;

	// Get chain's polar coordinates
	curposr = pow( posx[i]*posx[i] + posy[i]*posy[i] , 0.5 ) ;

	// Drift velocity from force
	if ( EqlFlag== 1 ) {

	  // Get force components at current position (nearest neighbours)
	  indr = (int) round((FNr-1)*curposr/AnteRadius);
	  indz = (int) round((FNz-1)*posz[i]/(AnteWidth+WallWidth));
	  curFr = Fr[indr + (FNr-1)*indz];
	  curFz = Fz[indr + (FNr-1)*indz];
	  //	  printf("curFz: %f\n",curFz);

	  // Convert radial force to cartesian
	  curFx = curFr * cos( atan2(posy[i],posx[i]) );
	  curFy = curFr * sin( atan2(posy[i],posx[i]) );

	  // Overdamped drift velocity
	  //if (chln[i]*curFz*dt > 1) {printf("curFz: %f\n\n",curFz);}
	  posx[i] = posx[i] + chln[i]*curFx*dt;
	  posy[i] = posy[i] + chln[i]*curFy*dt;
	  posz[i] = posz[i] + chln[i]*curFz*dt;
	}

	// BC 1: Reflective walls
	if ( posz[i] < WallHalfWidth ) { posz[i] = posz[i] + 2.0*(WallHalfWidth-posz[i]); }
	if ( posz[i] > AnteWidth+WallHalfWidth ) { posz[i] = posz[i] - 2.0*(posz[i]-AnteWidth-WallHalfWidth); }
	if ( curposr > AnteRadius ) { 
	  posx[i] = posx[i] - 2.0*(curposr-AnteRadius)*cos(atan2(posy[i],posx[i])); 
	  posy[i] = posy[i] - 2.0*(curposr-AnteRadius)*sin(atan2(posy[i],posx[i])); 
	}

	// BC 2: If near pore, if pore empty, then captured
	DistToPore = pow( pow(porex-posx[i],2.0)+pow(porey-posy[i],2.0)+pow(porez-posz[i],2.0) ,0.5);
	if ( (EqlFlag==1) && (DistToPore<Radius_PoreCapture) && (Get_NthBinaryBit(PoreState,trns[i])==0) ) {
	  stat[i] = stat[i] + (int)round(TransTime(chln[i])/dt); // Freeze chain for a certain number of timesteps
	  //PoreState = PoreState + (int)pow(2,trns[i]); // Fill pore
	  posz[i] = AnteWidth + WallWidth; posx[i] = 0.0; posy[i] = 0.0; // Position in pore
	}

      } // IF CHAIN IS MOVING
      else if (stat[i]==1) {
	  // Release from pore
	  stat[i]--;
	  //	  PoreState = PoreState - (int) pow(2,trns[i]);
	  // Position after pore
	  posz[i] = WallHalfWidth;
	  posx[i] = 0.0;
	  posy[i] = 0.0;
	  trns[i]++;
      } // IF TRANSLOCATION IS ENDING
      else {stat[i]--;}

    } // FOR EACH CHAIN
    if ( (EqlFlag==1) && ((nt%binfreq)==0) ) {fprintf(fbin,"\n");}
  } // WHILE NOT FINISHED
  printf("\n");

  return 0;
}










////////

float TransTime(float ChainLength) {

  // Preliminary approximate form for translocation times
  float TransTime=0;

  // snp
  // Easy: TransTime = 2.83*pow(ChainLength,1.50);
  // Better: log(tau) ~ Gauss ( 2.43 N^0.26, 1.19 N^{-0.35} )
  float rand0to1A = ((float)rand()/RAND_MAX);
  float rand0to1B = ((float)rand()/RAND_MAX);
  float randgaus = pow( -2*log(rand0to1A) ,0.5) * cos(2*3.14159*rand0to1B);
  float logTransTime = 1.19*pow(ChainLength,-0.35)*randgaus + 2.83*pow(ChainLength,0.26);
  TransTime = exp(logTransTime);

  /* // cnp */
  /* if ( ChainLength == 25 ) { TransTime = 1.6e4; } */
  /* if ( ChainLength == 50 ) { TransTime = 9e3; } */
  /* if ( ChainLength == 100 ) { TransTime = 1.6e4; } */
  /* if ( ChainLength == 200 ) { TransTime = 1.8e4; } */

  if ( TransTime == 0 ) { printf("Error! No ChainLength detected!\n"); }

  return TransTime;

}


float GetChainD(float ChainLength) {

  // Ignores hydrodynamic effects
  return 1.0/ChainLength;

}


int Get_NthBinaryBit(int Number, int N) {

  // N starts at 0
  return (int) floor( Number / pow(2, N) ) % 2;

}




