import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
def myFourthMoment(a):
    return np.mean( (a - np.mean(a))**4 )



casename="global-enh-baseline"



# Make plots folder if required
plotdir = "plots/"
if not os.path.exists(plotdir):
    os.makedirs(plotdir)


# Read summary dataframe
df_full = pd.read_csv("summary_"+casename+".csv",index_col=0)
df_full['N'] = df_full['N'].astype(int)
df_full['rseed'] = df_full['rseed'].astype(int)
df_full['nevent'] = df_full['nevent'].astype(int)
df_full['s_lt'] = df_full['s_lt'].astype(int)
df_full['s_fp'] = df_full['s_fp'].astype(int)

# Remove hairpins
hairpin_cutoff = 5
casename = casename + "-%s"%hairpin_cutoff
df = df_full[(df_full['s_lt']<=hairpin_cutoff)|(df_full['s_lt']>=(df_full['N']-hairpin_cutoff))]

# Group by N
dfN = df.groupby('N')


### Analysis 1
### tau, sigma, cv
tausN = dfN['tau']
n     = tausN.count()

tau   = tausN.mean()
sigma = tausN.std()
cv    = sigma/tau
mu4   = tausN.apply(myFourthMoment)

se_tau   = tausN.std()/np.sqrt(n)
se_var   = np.sqrt( (1./n) * (mu4 - ((n-3)/(n-1))*sigma**4) ) # https://tinyurl.com/hf7e8ag
se_sigma = se_var / (2.*sigma)
se_cv    = cv * ( (se_sigma/sigma) + (se_tau/tau) )

for varname in ['tau','sigma']:
    var = eval(varname)
    var.plot(yerr=eval('se_'+varname),
             logx=True,logy=True,
             xlim=[var.index.min()*0.8,var.index.max()*1.2],
             ylim=[var.min()*0.8,var.max()*1.2])
    plt.xlabel('N')
    plt.ylabel(varname)
    plt.savefig(plotdir+casename+'_nohp_'+varname+'.png')
    #plt.show()
    plt.clf()
for varname in ['cv']:
    var = eval(varname)
    var.plot(yerr=eval('se_'+varname),
             xlim=[var.index.min()-5,var.index.max()+5],
             ylim=[var.min()*0.8,var.max()*1.2])
    plt.xlabel('N')
    plt.ylabel(varname)
    plt.savefig(plotdir+casename+'_nohp_'+varname+'.png')
    #plt.show()
    plt.clf()



# ### Analysis 2
# ### tau distributions
# for N, taudist in df_full.groupby('N')['tau']:
#     taudist.hist(grid=False)
#     plt.xlabel('tau')
#     plt.title('N=%d'%N)
#     #plt.show()
#     plt.savefig(plotdir+casename+'_hp_'+'taudist_%d.png'%N)
#     plt.clf()
# df_full.hist(column='tau',by='N')
# plt.savefig(plotdir+casename+'_hp_'+'taudist_all.png')
# plt.clf()

# for N, taudist in tausN:
#     taudist.hist(grid=False)
#     plt.xlabel('tau')
#     plt.title('N=%d'%N)
#     #plt.show()
#     plt.savefig(plotdir+casename+'_nohp_'+'taudist_%d.png'%N)
#     plt.clf()
# df.hist(column='tau',by='N')
# plt.savefig(plotdir+casename+'_nohp_'+'taudist_all.png')
# plt.clf()


### Analysis 3
### Rg(N)
dfN['Rgx_lt'].mean().plot()
dfN['Rgy_lt'].mean().plot()
dfN['Rgz_lt'].mean().plot()
df['Rgx_lt'] = (df['Rgx_lt']**2+df['Rgy_lt']**2+df['Rgz_lt']**2)**0.5
Rg = df.groupby('N')['Rgx_lt'].mean()
plt.plot(Rg)
# Polymer contour length
Nwlc = np.linspace(5,250,1e3) # Number of bp
LC = Nwlc # sigma
LP = 6 #sigma
RG2 = (1./3.)*LP*LC - LP**2 + (2.*LP**3/LC)*( 1 - (LP/LC)*( 1 - np.exp(-LC/LP) ) )
RG = RG2**0.5
# Plot
plt.plot(LC, RG/3.**0.5, 'k--')
plt.plot(LC, RG, 'k--')
plt.xlabel('N')
plt.ylabel('Rg')
#plt.show()
plt.savefig(plotdir+casename+'_nohp_'+'Rg.png')
plt.clf()

Rgstd = df.groupby('N')['Rgx_lt'].std()
plt.plot(Rgstd,'.-')
plt.xlabel('N')
plt.ylabel('stddev(Rg)')
#plt.show()
plt.savefig(plotdir+casename+'_nohp_'+'stddevRg.png')
plt.clf()
