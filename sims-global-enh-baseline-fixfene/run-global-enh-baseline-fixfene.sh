#!/bin/bash

rseed_suffix=123456
mkdir -p logs
mkdir -p data
time=2400m
mpp="1g"
vis=0
filename="global-enh-baseline-fixfene"
jobname="global-enh-baseline-fixfene"


for rseed_prefix in {00..99}
do
    for N in 10 15 25 35 50 60 68 75 100 150 200  
    do
    mkdir -p data/N_${filename}_$N
    mkdir -p logs/N_${filename}_$N
    rseed=${N}${rseed_prefix}${rseed_suffix}
    sqsub --mpp=$mpp -q serial -o logs/N_${filename}_$N/log_$rseed.log -r $time -j "$jobname" ~/espresso-${filename}/Espresso ${filename}.tcl $N $rseed $filename $vis
    done
done






