import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# Select case
rante = float(sys.argv[1])
zante = float(sys.argv[2])
rmax = rante
zmax = zante 
Nr = int(sys.argv[3])
Nz = int(sys.argv[4])

# Load data
u = np.loadtxt('data/soln_%.1f_%.1f_%03d_%03d.dat'%
               (rante,zante,Nr,Nz))
U = np.resize(u,(Nz,Nr))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)

# Double-side everything
UU = np.concatenate((np.fliplr(U),U),axis=1)
RR = np.concatenate((-np.fliplr(R),R),axis=1)
ZZ = np.concatenate((np.fliplr(Z),Z),axis=1)
EEr = np.concatenate((-np.fliplr(Er),Er),axis=1)
EEz = np.concatenate((np.fliplr(Ez),Ez),axis=1)

# Plot
fig = plt.figure()
ax = plt.axes()
ax.pcolor(R,Z,U,vmin=-1.,vmax=1.)

#ax = plt.axes(projection='3d')
#ax.plot_surface(RR, ZZ, UU, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)

# ax.pcolor(RR,ZZ,UU,vmin=-1.,vmax=1.)
# ax.grid()
# V = np.linspace(0.9,1.0,30)
# V = np.concatenate((-V[::-1],V),axis=1)
# ax.contour(RR,ZZ,UU,V,colors='k',lw=6)
# ax.contour(R,Z,U,V,colors='k',lw=6)

#Streamplot doesn't work on uneven grids
ax.streamplot(RR,ZZ,EEr,EEz)

# Plot
plt.xlim([0,rmax])
plt.ylim([0,zmax])
plt.show()



plt.plot(z,U[:,0])
plt.show()

plt.plot(z,Ez[:,0])
plt.show()

