#include "laplace-in-cylinder.h"


int main(int argc, char* argv[]) {

  ////////////////////////////////////////////////////////////////
  //// Define domain, mesh, CLI
  if ( argc != 5 ) { printf("%s [rante] [zante] [Nr] [Nz] \n",argv[0]); return 1; }
  double rante = strtof(argv[1],NULL);     // Radius of antechamber
  double zante = strtof(argv[2],NULL);     // Width of antechamber
  double rmax = rante;
  double zmax = zante;
  int Nr = strtol(argv[3],NULL,10);       // Number of r nodes
  int Nz = strtol(argv[4],NULL,10);       // Number of z nodes
  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Set up A,f,u and initialize w/o BCs
  // Initialize operator A, RHS vector F, and solution U
  double** A = malloc(sizeof(double*)*Nz*Nr); 
  double* f = malloc(sizeof(double)*Nz*Nr); 
  double* u = malloc(sizeof(double)*Nz*Nr);
  for (int i=0; i<Nz*Nr; i++) {
    A[i] = malloc(sizeof(double)*Nz*Nr);
    for (int j=0; j<Nz*Nr; j++) { A[i][j] = 0; }
    f[i] = 0; u[i] = 0;
  }
  // R mesh
  double* dr = malloc(sizeof(double)*(Nr-1));
  for (int i=0; i<Nr-1; i++) { dr[i] = rmax/(Nr-1); }
  // Z mesh
  double* dz = malloc(sizeof(double)*(Nz-1));
  for (int i=0; i<Nz-1; i++) { dz[i] = zmax/(Nz-1); }
  // Fill operator, ignoring boundaries
  FillOperatorWoBcs(A,Nz,Nr,dz,dr);
  ////

  ////////////////////////////////////////////////////////////////
  //// Define geometry, BCs
  //// All conditions are homogeneous by default

  //// Simple Model:
  //// Small pore is a constant voltage mesh point
  //// Filter is a constant voltage wall

  // Indices
  int k_r0; int k_rmax;
  int k_z0; int k_zmax;
  int k_tpore;
  int k_rpore; int k_inpore;

  // Smoothness condition for cylindrical coordinates at r=0
  // Apply homogenous Newmann BCs in r for (0<z<zmax,r=0)
  for (int i=1; i<Nz-1; i++) {
    k_r0 = i*Nr + 0;
    SetBC_NewmannR(A,Nz,Nr,k_r0,dr,+1);
  }

  // Wall of Voltage
  // Apply non-homogeneous Dirichlet BCs for (z=zmax,all r)
  for (int j=0; j<Nr; j++) {
    k_zmax = (Nz-1)*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_zmax);
    f[k_zmax] = -1.;
  }

  // Wall of Antechamber
  // Apply homogenous Newmann BCs in r for (0<z<zmax,r=rmax)
  for (int i=1; i<Nz-1; i++) {
    k_rmax = i*Nr + (Nr-1);
    SetBC_NewmannR(A,Nz,Nr,k_rmax,dr,-1);
  }

  // Wall of Nanopore
  // Apply homogenous Newmann BCs in z for (z=0,0<r<=rmax)
  for (int j=2; j<Nr; j++) {
    k_tpore = 0*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k_tpore,dz,+1);
  }

  // Nanopore Voltage
  // Apply homogeneous Dirichlet BCs for (z=0,0<=r<1)
  for (int j=0; j<2; j++) {
    k_inpore = 0*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_inpore);
    f[k_inpore] = 0.;
  }

  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Solve, output, exit
  // Solve using LAPACK
  SolveSystem(A,u,f,Nz,Nr);
  // Output
  char fout_name[256]; sprintf(fout_name,"data/soln_%.1f_%.1f_%03d_%03d.dat",
			       rante,zante,Nr,Nz);
  FILE* fout =fopen(fout_name,"w");
  for (int i=0; i<Nz*Nr; i++) {
    fprintf(fout,"%.8e\n",u[i]);
  }
  // Release Memory
  fclose(fout); free(f); free(u); 
  for (int i=0; i<Nz*Nr; i++) {
    free(A[i]);
  } free(A);
  // Exit
  return 0;
  ////
}
