import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 400



#######################################################################


def main():

    ############################################################

    # Global analysis parameters
    hairpin_cutoff = 0

    # Make plots folder if required
    plotdir = "plots/"
    if not os.path.exists(plotdir):
        os.makedirs(plotdir)

    # Make comparison plot figures
    ftau, axtau       = plt.subplots()
    fsigma, axsigma   = plt.subplots()
    fcv, axcv         = plt.subplots()
    frg, axrg         = plt.subplots()
    frgr, axrgr       = plt.subplots()
    frgsig, axrgsig   = plt.subplots()
    frgrsig, axrgrsig = plt.subplots()

    ### Experiment
    expdata = np.loadtxt('experimental-data.txt',delimiter=',')
    exp_Ns = expdata[:,0]
    exp_taus = expdata[:,1]
    exp_sigmas = expdata[:,2]
    exp_cvs = exp_sigmas/exp_taus
    axtau.plot(exp_Ns,exp_taus,'g*',label="Experimental")
    axsigma.plot(exp_Ns,exp_sigmas,'g*',label="Experimental")
    axcv.plot(exp_Ns,exp_cvs,'g*',label="Experimental")

    ############################################################

    # Loop through desired simulation cases
    #casenames=["global-enh-fixfene","global-enh-baseline-fixfene"]
    casenames=["global-enh-fixfene","capture"]
    for casename in casenames:

        # Read summary dataframe
        df_full = NPF_LoadDataFrame("sims-%s/summary_%s.csv"%(casename,casename))
        df = NPF_RemoveHairpins(df_full,hairpin_cutoff)

        # # Rate of hairpins
        # fighptest, axhptest = plt.subplots()
        # for hptest_hairpin_cutoff in [0,5,10,200]:
        #     df_hptest = NPF_RemoveHairpins(df_full,hptest_hairpin_cutoff)
        #     df_hptest[['Nbp','s_lt']].groupby('Nbp').count().plot(y='s_lt',ax=axhptest,label='|s_lt - [0,N]|<%d'%hptest_hairpin_cutoff)
        # axhptest.set_ylabel('# Events thaat Thread Near an End')
        # fighptest.savefig(plotdir+casename+'_hptest%d.png'%hptest_hairpin_cutoff)
        # plt.close(fighptest)

        # # Compare hairpin threshold effect on cv
        # fighpcomp, axhpcomp = plt.subplots()
        # for hpcomp_hairpin_cutoff in range(6):
        #     df_hpcomp = NPF_RemoveHairpins(df_full,hpcomp_hairpin_cutoff)
        #     a = NPF_AnalyseTau(df_hpcomp)
        #     TauAnalysis_PlotCV(a,"Cutoff = %d"%hpcomp_hairpin_cutoff,axhpcomp)
        # fighpcomp.savefig(plotdir+casename+'_hpcomp%d.png'%hpcomp_hairpin_cutoff)
        # plt.close(fighpcomp)

        # Analyse tau
        a = NPF_AnalyseTau(df)
        TauAnalysis_PlotTau(a,casename,axtau)
        TauAnalysis_PlotSigma(a,casename,axsigma)
        TauAnalysis_PlotCV(a,casename,axcv)
    
        # Plot Rgs
        NPF_PlotRg(df,casename,axrg)
        rgr = NPF_PlotRgr(df,casename,axrgr)
        NPF_PlotRgSig(df,casename,axrgsig)
        NPF_PlotRgrSig(df,casename,axrgrsig)

        # Plot Ncis
        fncis, axncis = plt.subplots()
        ncis = NPF_PlotNcis(df,casename,axncis)
        fncis.savefig(plotdir+casename+'_ncis_nohp%d.png'%hairpin_cutoff)
        plt.close(fncis)

        # # Plot tau distributions
        # fhist, axhist = plt.subplots(figsize=(12.8,9.6))
        # df.hist(column='tau',by='N',ax=axhist)
        # fhist.savefig(plotdir+casename+'_taudists_nohp%d.png'%hairpin_cutoff)
        # plt.close(fhist)

        # Plot s_lt distributions
        fhist, axhist = plt.subplots(figsize=(12.8,9.6))
        df.hist(column='s_lt',by='N',ax=axhist,bins=200)
        fhist.savefig(plotdir+casename+'_sltdists_nohp%d.png'%hairpin_cutoff)
        plt.close(fhist)
        # for N, group in df.groupby('N'):
        #     thehist = np.histogram(group['s_lt'])
        #     pd.DataFrame([thehist[0],thehist[1][1:]]).to_csv(plotdir+casename+'_sltdist%d_nohp%d.csv'%(N,hairpin_cutoff),index=False,header=False)
        
        # # Plot tau vs Rg distributions
        # taurgs = df.groupby('N').plot.scatter(x='Rg_lt',y='tau')
        # for N, ax in taurgs.iteritems():
        #     ax.figure.savefig(plotdir+casename+'_taurg%d_nohp%d.png'%(N,hairpin_cutoff))
        #     plt.close(ax.figure)

        # # Plot tau vs Rgr distributions
        # taurgrs = df.groupby('N').plot.scatter(x='Rgr_lt',y='tau')
        # for N, ax in taurgrs.iteritems():
        #     ax.figure.savefig(plotdir+casename+'_taurgr%d_nohp%d.png'%(N,hairpin_cutoff))
        #     plt.close(ax.figure)

        # # Print some data to csv
        # a.to_csv(plotdir+casename+'_taudata_nohp%d.csv'%hairpin_cutoff)
        # df[df['N']==100][['tau','Rg_lt']].to_csv(plotdir+casename+'_taurg100_nohp%d.csv'%hairpin_cutoff)
        # rgr.to_csv(plotdir+casename+'_rgr_nohp%d.csv'%hairpin_cutoff)
        # # s_lt dists?
        # ncis.to_csv(plotdir+casename+'_ncis_nohp%d.csv'%hairpin_cutoff)

    ############################################################

    # Add WLC to Rg plots
    Nbp_WLC = np.linspace(5,4000,1e4)    # bp
    LC_WLC = Nbp_WLC*0.34 # nm
    LP_WLC = 6 * 5.           # nm
    RG2_WLC = ((1./3.)*LP_WLC*LC_WLC -
               LP_WLC**2 + ((2.*LP_WLC**3/LC_WLC) *
                            (1-(LP_WLC/LC_WLC)*(1-np.exp(-LC_WLC/LP_WLC))) ) )
    RG_WLC = RG2_WLC**0.5
    # Plot
    axrg.plot(Nbp_WLC, RG_WLC, 'k--', label='WLC')                       # WLC Rg
    #axrg.plot(Nbp_WLC, RG_WLC/(3.**0.5), 'k--')                         # WLC Rgx
    #axrgr.plot(Nbp_WLC, RG_WLC/(1.2**0.5), 'k--', label='WLC/sqrt(1.2)') # WLC Rgr (Teraoka, pg 28)
    #axrg.plot(Nbp_WLC, 0.4*RG_WLC/(1.2**0.5), 'k--')                    # WLC Rgp (Teraoka, pg 28)
    axrg.legend(loc='upper left')
    axrgr.legend(loc='upper left')
    
    # Fix log-log axes that might not be centered well
    FixLogLogAxes(axtau)
    FixLogLogAxes(axsigma)
    
    # Save and close comparison plots
    ftau.savefig(plotdir+"tau_nohp%d.png"%hairpin_cutoff)
    fsigma.savefig(plotdir+"sigma_nohp%d.png"%hairpin_cutoff)
    fcv.savefig(plotdir+"cv_nohp%d.png"%hairpin_cutoff)
    frg.savefig(plotdir+"rg_nohp%d.png"%hairpin_cutoff)
    frgr.savefig(plotdir+"rgr_nohp%d.png"%hairpin_cutoff)
    frgsig.savefig(plotdir+"rgsig_nohp%d.png"%hairpin_cutoff)
    frgrsig.savefig(plotdir+"rgrsig_nohp%d.png"%hairpin_cutoff)
    plt.close(ftau)
    plt.close(fsigma)
    plt.close(fcv)
    plt.close(frg)
    plt.close(frgr)

    # Catch any remaining figure
    plt.close("all")



#######################################################################






def myFourthMoment(a):
    return np.mean( (a - np.mean(a))**4 )

def FixLogLogAxes(ax):
    minx=1e6;miny=1e6;maxx=0;maxy=0
    for line in ax.lines:
        if (minx>np.min(line.get_xdata())):
            minx=np.min(line.get_xdata())
        if (miny>np.min(line.get_ydata())):
            miny=np.min(line.get_ydata())
        if (maxx<np.max(line.get_xdata())):
            maxx=np.max(line.get_xdata())
        if (maxy<np.max(line.get_ydata())):
            maxy=np.max(line.get_ydata())
    ax.set_xlim([minx*0.5,maxx*2.])
    ax.set_ylim([miny*0.5,maxy*2.])

def NPF_LoadDataFrame(csvname):
    # Load an NPF DataFrame from CSV form
    # Input: CSV filename with path
    # Output: NPF DataFrame with dtypes specified
    df_full = pd.read_csv(csvname,index_col=0)
    df_full['N']      = df_full['N'].astype(int)
    df_full['rseed']  = df_full['rseed'].astype(int)
    df_full['nevent'] = df_full['nevent'].astype(int)
    df_full['s_lt']   = df_full['s_lt'].astype(int)
    df_full['s_fp']   = df_full['s_fp'].astype(int)
    df_full['N_cis']  = df_full['N_cis'].astype(int)
    # Select lengthscale
    sig_exp = 5.0 # nm
    # Set chain lengths in Nbp
    df_full['Nbp']     = (df_full['N'].astype(float)*sig_exp/0.34).round().astype(int)
    df_full['Nbp_cis'] = (df_full['N_cis'].astype(float)*sig_exp/0.34).round().astype(int)
    # Set Rgs in nm
    rgnames = ['Rgx_lt','Rgy_lt','Rgz_lt','Rg_lt','Rgr_lt','Rgp_lt']
    df_full[rgnames] = df_full[rgnames] * sig_exp
    # Set tau in microseconds
    sig_exp_m = sig_exp*10**(-9); mu_exp = 0.6*10**(-8); V_exp = 0.200; N_tune = 80
    L_exp  = N_tune*sig_exp_m; D_exp = 2.38*10**(-12)/(L_exp*10**6)**0.608
    field_factor = 3.3; V_sim = field_factor*mu_exp*V_exp/(N_tune*D_exp)
    t_sim = (field_factor*sig_exp_m**2/(N_tune*D_exp)) * 1e6 # mus
    df_full['tau'] = df_full['tau'] * t_sim
    return df_full

def NPF_RemoveHairpins(df_full, hairpin_cutoff):
    # Remove cases that don't thread near an end
    # Input: NPF DataFrame, Hairpin Cutoff
    # Output: NPF DataFrame
    df = df_full[(df_full['s_lt']<=hairpin_cutoff)|(df_full['s_lt']>=(df_full['N']-hairpin_cutoff))]
    return df

def NPF_AnalyseTau(df):
    # Get tau,sigma,cv vs N with standard errors
    # Input: NPF DataFrame
    # Output: TauAnalysis DataFrame
    a = df[['Nbp','tau']].groupby('Nbp')['tau'].agg({'n'        : 'count',
                                                     'tau'      : 'mean',
                                                     'sigma'    : 'std',
                                                     'mu4'      : myFourthMoment})
    a['cv']       = a['sigma']/a['tau']
    a['se_tau']   = a['sigma']/a['n']**0.5
    a['se_sigma'] = ((1./(2.*a['sigma'])) * # https://tinyurl.com/hf7e8ag
                     np.sqrt( (1./a['n']) * (a['mu4'] - ((a['n']-3)/(a['n']-1))*a['sigma']**4) ))
    a['se_cv']    = a['cv'] * ( (a['se_sigma']/a['sigma']) + (a['se_tau']/a['tau']) )
    return a[['n','tau','sigma','mu4','cv','se_tau','se_sigma','se_cv']]

def TauAnalysis_PlotTau(a,casename,ax):
    # Plots the tau from NPF_AnalyseTau
    # Input: TauAnalysis DataFrame
    # Output: None (Adds plot to ax)
    tau = a['tau']
    tau.plot(ax=ax,
             yerr=a['se_tau'],
             logx=True,logy=True,
             xlim=[tau.index.min()*0.8,tau.index.max()*1.2],
             label=casename,legend=True)
    ax.legend(loc='upper left')
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'$\tau$')

def TauAnalysis_PlotSigma(a,casename,ax):
    # Plots the sigma from NPF_AnalyseTau
    # Input: TauAnalysis
    # Output: None (Adds plot to ax)
    sigma = a['sigma']
    sigma.plot(ax=ax,
               yerr=a['se_sigma'],
               logx=True,logy=True,
               xlim=[sigma.index.min()*0.8,sigma.index.max()*1.2],
               label=casename,legend=True)
    ax.legend(loc='upper left')
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'$\sigma$')

def TauAnalysis_PlotCV(a,casename,ax):
    # Plots the cv from NPF_AnalyseTau
    # Input: TauAnalysis
    # Output: None (Adds plot to ax)
    cv = a['cv']
    cv.plot(ax=ax,
            yerr=a['se_cv'],
            xlim=[0,cv.index.max()+cv.index.min()],
            label=casename,legend=True)
    ax.legend(loc='upper right')
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'$\sigma/\tau$')

def NPF_PlotRg(df,casename,ax):
    # Plots the Rg of an NPF DataFrame 
    # Input: NPF DataFrame
    # Output: None (Adds plots to ax)
    rg = df.groupby('Nbp')['Rg_lt'].agg(('count','mean','std'))
    rg['mean'].plot(ax=ax,
                    yerr=rg['std']/(rg['count'])**0.5,
                    xlim=[0,rg.index.max()+rg.index.min()],
                    label=casename)
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'Total $R_g$ in nm')

def NPF_PlotRgr(df,casename,ax):
    # Plots the Rgr of an NPF DataFrame 
    # Input: NPF DataFrame
    # Output: None (Adds plots to ax)
    rgr = df.groupby('Nbp')['Rgr_lt'].agg(('count','mean','std', lambda x : x.std()/(x.count())**0.5))
    rgr['mean'].plot(ax=ax,
                     yerr=rgr['std']/(rgr['count'])**0.5,
                     xlim=[0,rgr.index.max()+rgr.index.min()],
                     label=casename)
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'Radial $R_g$ in nm')
    return rgr

def NPF_PlotRgSig(df,casename,ax):
    # Plots the stddev of Rg of an NPF DataFrame 
    # Input: NPF DataFrame
    # Output: None (Adds plots to ax)
    rg = df.groupby('Nbp')['Rg_lt'].agg({'n'   : 'count',
                                         'std' : 'std',
                                         'mu4' : myFourthMoment})
    rg['se_std'] = ((1./(2.*rg['std'])) * # https://tinyurl.com/hf7e8rgg
                    np.sqrt( (1./rg['n']) * (rg['mu4'] - ((rg['n']-3)/(rg['n']-1))*rg['std']**4) ))
    rg['std'].plot(ax=ax,
                   yerr=rg['se_std'],
                   xlim=[0,rg.index.max()+rg.index.min()],
                   label=casename,legend=True)
    ax.legend(loc='upper left')
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'Std Dev of Total $R_g$')

def NPF_PlotRgrSig(df,casename,ax):
    # Plots the stddev of Rgr of an NPF DataFrame 
    # Input: NPF DataFrame
    # Output: None (Adds plots to ax)
    rgr = df.groupby('Nbp')['Rgr_lt'].agg({'n'   : 'count',
                                           'std' : 'std',
                                           'mu4' : myFourthMoment})
    rgr['se_std'] = ((1./(2.*rgr['std'])) * # https://tinyurl.com/hf7e8rgrg
                     np.sqrt( (1./rgr['n']) * (rgr['mu4'] - ((rgr['n']-3)/(rgr['n']-1))*rgr['std']**4) ))
    rgr['std'].plot(ax=ax,
                    yerr=rgr['se_std'],
                    xlim=[0,rgr.index.max()+rgr.index.min()],
                    label=casename,legend=True)
    ax.set_xlabel('N (bp)')
    ax.set_ylabel(r'Std Dev of Radial $R_g$')

def NPF_PlotNcis(df,casename,ax):
    # Plots the probability of and mean length of non-zero tails
    # Input: NPF DataFrame
    # Output: None (Adds plots to ax)
    ncis = df.groupby('Nbp')['Nbp_cis'].agg({'Ptail' : lambda x : float(x[x>0].count())/float(x.count()),
                                           'Ltail' : lambda x : x[x>0].mean()})
    ncis = ncis.fillna(value=0)
    ncis['Ltail'].plot(ax=ax,
                       xlim=[0,ncis.index.max()+ncis.index.min()],
                       ylim=[0,ncis.index.max()],
                       label="Length",
                       style='k-')
    ax.set_ylabel('Average Length of Non-Zero Tails in bp')
    ncis['Ptail'].plot(ax=ax,
                       secondary_y=True,
                       label="Probability",
                       style='r--')
    ax.right_ax.set_ylabel('Probability of Non-Zero Tail')
    ax.right_ax.set_ylim([0,1])
    # Legend
    lines = ax.get_lines() + ax.right_ax.get_lines()
    ax.legend(lines, [l.get_label() for l in lines], loc='upper left')
    ax.set_xlabel('N (bp)')
    return ncis
    
def TestPlot(df):
    N0 = 200
    nbins = 10

    dfN = df[df['N']==N0][['tau','Rgr_lt']]
    sigma_v_rgr = dfN.groupby(pd.cut(dfN['Rgr_lt'],np.linspace(dfN['Rgr_lt'].min(),dfN['Rgr_lt'].max(),nbins))).agg({'tau' : 'std', 'Rgr_lt' : 'mean'})
    cv_v_rgr = dfN.groupby(pd.cut(dfN['Rgr_lt'],np.linspace(dfN['Rgr_lt'].min(),dfN['Rgr_lt'].max(),nbins))).agg({'tau' : lambda x : x.std()/x.mean(), 'Rgr_lt' : 'mean'})

    plt.plot(sigma_v_rgr['Rgr_lt'],sigma_v_rgr['tau'])
    plt.xlabel(r'Radial $R_g$ in nm')
    plt.ylabel(r'$\sigma$ in $\mu$s')
    plt.title('N = %d'%N0)
    plt.show()
    
    plt.plot(cv_v_rgr['Rgr_lt'],cv_v_rgr['tau'])
    plt.xlabel(r'Radial $R_g$ in nm')
    plt.ylabel(r'$\sigma/\tau$')
    plt.title('N = %d'%N0)
    plt.show()
    


if __name__ == "__main__":
    main()
